# 1000x Page
Page for the 1000x Token and to interact with the ThousandXSWAP contract. It's simple interface to swap MintMe.com Tokens.

## Requrements
 - Node v15.12.0 (v16 will fail)
 - Runing [1000xAPI](https://gitlab.com/musdasch/1000xapi) Server 

## Demo Page
 - [Demo Page](https://www.1000x.ch/)

## Project setup
```
npm install
```
Update the property apiURL and thousandXSWAPAddress in `src/setting/options.js`.

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## License
Copyright © 2022 musdasch <musdasch@protonmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.